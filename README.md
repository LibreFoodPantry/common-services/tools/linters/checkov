# checkov

[checkov](https://www.checkov.io/) scans for cloud infrastructure
misconfigurations.

## Using in the Pipeline

To enable checkov in your project's pipeline add
`checkov` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "checkov"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the [Checkov Visual Studio Code extension](https://marketplace.visualstudio.com/items?itemName=Bridgecrew.checkov)
for Visual Studio Code.

**Note from the page for the extension:** *"To use this checkov-vscode
extension, you will need to create a free account at bridgecrew.cloud using
your e-mail, the plugin uses Bridgecrew.cloud's fixes API to analyze and
produce code fixes, and enrich the results provided into VSCode. Please notice
bridgecrew privacy policy for more details on collected data when using
bridgecrew application. To generate fixes, files found to have triggered
checkov violations are made available to the fixes API for the sole purpose
of generating inline fixes code recommendations."*

## Using Locally in Your Development Environment

checkov can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run --tty --volume "${PWD}:/workdir" --workdir /workdir 
    bridgecrew/checkov --directory /workdir
```

## Configuration

Each project may have a `.checkov.yaml` or `.checkov.yml` file which is used
for
[checkov configuration](https://github.com/bridgecrewio/checkov#configuration-using-a-config-file).

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
